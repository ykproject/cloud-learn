package com.yk9527.config;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GateWayConfig {
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("path_route_guonei",
                r -> r.path("/guonei")
                        .uri("http://news.baidu.com/guonei")).build();
        return routes.build();
    }

    /**
     * 地球半径，单位m
     */
    private static final double EARTH_RADIUS = 6378137;

    public static void main(String[] args) {
        //getS();
        //----POINT(119.551505,36.865699)
        //1.POINT(119.551505 36.8657)  0.11119468203080607
        //2.POINT(119.544893 36.863085) 656.1136144112786
        //3.POINT(119.543749 36.8637)  724.9073192545222
        System.out.println("getS1-1 "+getS1(119.551505,36.8657));
        System.out.println("getS1-2 "+getS1(119.544893,36.863085));
        System.out.println("getS1-3 "+getS1(119.543749,36.8637));
        //System.out.println("getS2-1 "+getS2(119.551505,36.8657));
        //System.out.println("getS2-2 "+getS2(119.544893,36.863085));
        //System.out.println("getS2-3 "+getS2(119.543749,36.8637));
        System.out.println("getS3-1 "+getS3(119.551505,36.8657));
        System.out.println("getS3-2 "+getS3(119.544893,36.863085));
        System.out.println("getS3-3 "+getS3(119.543749,36.8637));
        getS4(119.551505,36.8657);
        getS4(119.544893,36.863085);
        getS4(119.543749,36.8637);

    }

    private static double getS1(double lng,double lat) {
        // 经度
        double lng1 = Math.toRadians(119.551505);
        double lng2 = Math.toRadians(lng);
        // 纬度
        double lat1 = Math.toRadians(36.865699);
        double lat2 = Math.toRadians(lat);
        // 纬度之差
        double a = lat1 - lat2;
        // 经度之差
        double b = lng1 - lng2;
        // 计算两点距离的公式
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
                Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(b / 2), 2)));
        // 弧长乘地球半径, 返回单位: 米
        s =  s * EARTH_RADIUS;
        return s;
    }

    /**
     * 计算两个经纬度之间的距离
     * @param gpsFrom 第一个经纬度
     * @param gpsTo 第二个经纬度
     * @param ellipsoid 计算方式
     * @return 返回的距离，单位m
     */
    public static double getDistanceMeter(GlobalCoordinates gpsFrom, GlobalCoordinates gpsTo, Ellipsoid ellipsoid)
    {
        //创建GeodeticCalculator，调用计算方法，传入坐标系、经纬度用于计算距离
        GeodeticCurve geoCurve = new GeodeticCalculator().calculateGeodeticCurve(ellipsoid, gpsFrom, gpsTo);

        return geoCurve.getEllipsoidalDistance();
    }
    public static void getS4(double lng,double lat){
        double lon1 = 119.551505;
        double lat1 = 36.865699;
        double lon2 = lng;
        double lat2 = lat;

        GlobalCoordinates source = new GlobalCoordinates(lon1, lat1);
        GlobalCoordinates target = new GlobalCoordinates(lon2, lat2);

        double meter1 = getDistanceMeter(source, target, Ellipsoid.Sphere);
        //double meter2 = getDistanceMeter(source, target, Ellipsoid.WGS84);
        System.out.println("getS4 "+meter1);
        //System.out.println("getS4-WGS84 "+meter2);
    }

    /**
     * 转化为弧度(rad)
     */
    private static double rad(double d)
    {
        return d * Math.PI / 180.0;
    }

    private static final double EARTH_RADIUS11 = 6371000; // 平均半径,单位：m；不是赤道半径。赤道为6378左右
    public static double getS3(double lng,double lat) {
        // 经纬度（角度）转弧度。弧度用作参数，以调用Math.cos和Math.sin
        double radiansAX = Math.toRadians(119.551505); // A经弧度
        double radiansAY = Math.toRadians(36.865699); // A纬弧度
        double radiansBX = Math.toRadians(lng); // B经弧度
        double radiansBY = Math.toRadians(lat); // B纬弧度

        // 公式中“cosβ1cosβ2cos（α1-α2）+sinβ1sinβ2”的部分，得到∠AOB的cos值
        double cos = Math.cos(radiansAY) * Math.cos(radiansBY) * Math.cos(radiansAX - radiansBX)
                + Math.sin(radiansAY) * Math.sin(radiansBY);
//        System.out.println("cos = " + cos); // 值域[-1,1]
        double acos = Math.acos(cos); // 反余弦值
//        System.out.println("acos = " + acos); // 值域[0,π]
//        System.out.println("∠AOB = " + Math.toDegrees(acos)); // 球心角 值域[0,180]
        return EARTH_RADIUS11 * acos; // 最终结果
    }
}

