package com.ykOpenFeign80.controller;

import com.ykCloudLearn.CommonApi.entity.CommonResult;
import com.ykCloudLearn.CommonApi.entity.Payment;
import com.ykOpenFeign80.service.PaymentOpenFeignService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class PaymentOpenFeignController {

    @Value("${server.port}")
    String serverPort;

    @Resource
    private PaymentOpenFeignService paymentOpenFeignService;

    @GetMapping(value = "/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id)
    {
        CommonResult<Payment> payment = paymentOpenFeignService.getPaymentById(id);

        if(payment != null)
        {
            return new CommonResult(200,"查询成功,serverPort:  "+serverPort,payment);
        }else{
            return new CommonResult(444,"没有对应记录,查询ID: "+id,null);
        }
    }
}
