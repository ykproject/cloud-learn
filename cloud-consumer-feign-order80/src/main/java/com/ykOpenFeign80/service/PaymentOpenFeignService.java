package com.ykOpenFeign80.service;

import com.ykCloudLearn.CommonApi.entity.CommonResult;
import com.ykCloudLearn.CommonApi.entity.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "cloud-payment-service")
public interface PaymentOpenFeignService {
    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/testTimeout")
    String testTimeout();
}
