package com.yk80.cloudconsumerorder80;

import com.yk80.myRibbonRule.mySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@EnableEurekaClient
//name  与 OrderController PAYMENT_URL一致
@RibbonClient(name = "cloud-payment-service",configuration = mySelfRule.class)
//@RibbonClient(name = "CLOUD-PAYMENT-SERVICE",configuration = mySelfRule.class)
public class CloudConsumerOrder80Application {
    //12_消费者订单模块(上)
    public static void main(String[] args) {
        SpringApplication.run(CloudConsumerOrder80Application.class, args);
    }

}
