package com.yk80.myRibbonRule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//自定义ribbon负载均衡规则
@Configuration
public class mySelfRule {
    @Bean
    public IRule myRule(){
        return new RandomRule();
    }
}
