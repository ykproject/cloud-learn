package com.ykzk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class OrederZKMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrederZKMain80.class,args);
    }
}

