package com.yk7001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer//Eureka服务器
public class EurekaMain7001 {
    //  http://localhost:7001/  eureka 主页
    //20_Eureka集群环境构建
    public static void main(String[] args) {
        SpringApplication.run(EurekaMain7001.class, args);
    }
}
