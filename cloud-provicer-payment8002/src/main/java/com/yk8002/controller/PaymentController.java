package com.yk8002.controller;

import com.yk8002.service.PaymentService;
import com.ykCloudLearn.CommonApi.entity.CommonResult;
import com.ykCloudLearn.CommonApi.entity.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@Slf4j
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;
    @Resource
    private PaymentService paymentService;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment)
    {
        int result = paymentService.create(payment);
        log.info("*****插入结果："+result);

        if(result > 0)
        {
            return new CommonResult(200,"插入数据库成功,serverPort: "+serverPort,result);
        }else{
            return new CommonResult(444,"插入数据库失败",null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id)
    {
        Payment payment = paymentService.getPaymentById(id);

        if(payment != null)
        {
            return new CommonResult(200,"查询成功,serverPort:  "+serverPort,payment);
        }else{
            return new CommonResult(444,"没有对应记录,查询ID: "+id,null);
        }
    }

    @GetMapping(value = "/payment/lb")
    public String lb()
    {
        return serverPort;
    }

    //https://hk4e-api.mihoyo.com/event/gacha_info/api/getGachaLog?win_mode=fullscreen&authkey_ver=1&sign_type=2&auth_appid=webview_gacha&init_type=301&gacha_id=b4ac24d133739b7b1d55173f30ccf980e0b73fc1&lang=zh-cn&device_type=mobile&game_version=CNRELiOS3.0.0_R10283122_S10446836_D10316937&plat_type=ios&game_biz=hk4e_cn&size=20&authkey=aom4i3CsjE4LnwmL6uQkJvCMdZN9rvQaJFMqz9HlmAraVWVqr5nasnnOfuBtYkgYacM9YtVqX0%2BmWfvJM9D7ok%2BeXxmtMlMngIzUAk%2FF%2FI%2Fd0vKlR%2FAxKZZLnIWIa%2FbhVVyPwg5oQHwFKV1W3J3ADgctHWimbFiUefM%2BejIscjDsIETwXnhJSe3u4qV55mDEVJICz0tG%2BWl1QzlJUVzEbzVNw2LbGwFQ36vUDuvVaiHej6GPEHSDLDjCegIVOuj0F9OhpMII9u5Egerw%2FL2mua64eqf%2BmSR1z%2FWYnSUEjz6dwNdxvssUg%2B2DiZqyvx3aGX2YzE3dTVRyrtS17uh0LA%3D%3D&region=cn_gf01&timestamp=1664481732&gacha_type=200&page=1&end_id=0
    @GetMapping(value = "event/gacha_info/api/getGachaLog")
    public CommonResult<Payment> test(Map<String,String> params)
    {
        System.out.println("=============================");
        System.out.println(params.toString());
        return null;
    }
}
