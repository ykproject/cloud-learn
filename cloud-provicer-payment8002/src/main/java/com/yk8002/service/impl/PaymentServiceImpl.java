package com.yk8002.service.impl;

import com.yk8002.mapper.PaymentMapper;
import com.yk8002.service.PaymentService;
import com.ykCloudLearn.CommonApi.entity.Payment;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentMapper paymentMapper;

    public int create(Payment payment)
    {
        return paymentMapper.create(payment);
    }

    public Payment getPaymentById(Long id)
    {
        return paymentMapper.getPaymentById(id);
    }
}
