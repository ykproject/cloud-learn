package com.ykNacosConfig.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope //springcloud原生注解 支持Nacos的动态刷新功能。
public class ConfigClientController
{
    @Value("${config.info}")
    private String configInfo;
    @Value("${config.local.env}")
    private String configLocalEnv;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo+",local useable application:"+configLocalEnv;
    }
}
