package com.ykOrder84.service;

import com.ykCloudLearn.CommonApi.entity.CommonResult;
import com.ykCloudLearn.CommonApi.entity.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallBackService implements PaymentService{
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(44444,"服务降级返回,---PaymentFallbackService",new Payment(id,"errorSerial"));
    }
}
