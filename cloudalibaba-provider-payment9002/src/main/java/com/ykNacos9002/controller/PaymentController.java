package com.ykNacos9002.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope //springcloud原生注解 支持Nacos的动态刷新功能。
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    @Value("${config.info}")
    private String info;

    //http://localhost:9002/payment/nacos/1
    @GetMapping(value = "/payment/nacos/{id}")
    public String getPayment(@PathVariable("id") Integer id) {
        return "nacos registry, serverPort: "+ serverPort+"\t id"+id+",and config info:"+info;
    }

}
