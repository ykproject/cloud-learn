package com.yksentinel8401.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ykCloudLearn.CommonApi.entity.CommonResult;
import com.ykCloudLearn.CommonApi.entity.Payment;
import com.yksentinel8401.handler.CustomerBlockHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@Slf4j
public class FlowLimitController {
    @GetMapping("/testA")
    public String testA()
    {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "------testA";
    }

    @GetMapping("/testB")
    public String testB()
    {
        log.info(Thread.currentThread().getName()+"\t"+"...testB");
        return "------testB";
    }

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testResource",blockHandler = "testHandler")
    //testResource   sentinel的资源名  testHandler兜底方法
    public String testhotKey(@RequestParam(value = "pp",required = false) String pp,@RequestParam(value = "xx",required = false) String xx)
    {
        return "------testHotKey success";
    }

    public String testHandler(String pp, String xx, BlockException exception)
    {
        return "!!!!!!!!!!!!testHotKey is limit";
    }

    @GetMapping("/customer")
    @SentinelResource(value = "customerHandler",
            blockHandlerClass = CustomerBlockHandler.class,//处理BlockException的类
            blockHandler = "handlerBlockException1")//处理BlockException的的方法
    public CommonResult<Payment> customerBlockHandeler(){
        return new CommonResult<Payment>(200,"用户自定义接口",new Payment(111L,"serial123"));
    }


    //public static void main(String[] args) {
    //    int min = 100000;
    //    int max = 999999;
    //    Random random = new Random();
    //    int randomNumber = random.nextInt(max - min + 1) + min;
    //    System.out.println("Random 6-digit number: " + randomNumber);
    //}
}
