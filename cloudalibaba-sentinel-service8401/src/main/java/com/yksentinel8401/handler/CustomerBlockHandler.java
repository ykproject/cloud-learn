package com.yksentinel8401.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ykCloudLearn.CommonApi.entity.CommonResult;

public class CustomerBlockHandler {
    public static CommonResult handlerBlockException1(BlockException e){
        return new CommonResult(501,"this is handlerBlockException---1");
    }
    public static CommonResult handlerBlockException2(BlockException e){
        return new CommonResult(502,"this is handlerBlockException---2");
    }
}
